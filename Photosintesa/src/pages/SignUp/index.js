import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { ProfileSignUp } from "../../assets";
import React, { useState } from 'react';
import { Input } from "../../components/Atoms";
import { useSelector } from 'react-redux';
import { WARNA_BIRU } from "../../utils/constant";


const SignUp = (props) => {
    const globalState = useSelector(state => state);
    const [form, setForm] = useState({
        phone: '',
        email: '',
        password: ''
    });
  
    const sendData = () => {
        console.log('data yang dikirim: ', form);
    };

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    };
    return (
        
        <View style={styles.wrapper}>
            
        <Text style={styles.sign}>Create an Account</Text>
            <Image source={ProfileSignUp} style={styles.profile} />

        <View style={styles.space(60)}  />  
        <Input placeholder="Phone" value={form.phone} onChangeText={(value) => onInputChange(value, 'phone')}/>
        <View style={styles.space(15)}  /> 
        <Input placeholder="Email" value={form.email} onChangeText={(value) => onInputChange(value, 'email')}/> 
        <View style={styles.space(15)}  />
        <Input placeholder="Password" value={form.password} onChangeText={(value) => onInputChange(value, 'password')}
        secureTextEntry={true}
        /> 
        <View style={styles.space(40)}  />
        <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate('Login')}>
         <Text style={styles.getstarted} >
             Sign Up
         </Text> 
     </TouchableOpacity>  
        </View>
    )
}

export default SignUp

const styles = StyleSheet.create({
    sign: {
        color: '#FFFFFF',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 20,
        marginTop: 70,
        textAlign: 'center'
    },
    profile: {
        width: 120,
        height: 120,
        justifyContent: 'center',
        marginTop: 60,
        marginLeft: 130
    },
    wrapper: {
        padding: 20,
        flex: 1
    },
    button: {
        backgroundColor: WARNA_BIRU,
        borderRadius: 15,
        paddingVertical: 13,
        width: 350,
        alignSelf: 'center'
    },
    getstarted: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        color: '#FFFFFF',
        textAlign: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    
});
