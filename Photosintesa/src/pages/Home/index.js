import React from 'react'
import { StyleSheet, Text, View, ImageBackground, Dimensions, Image } from 'react-native'
import { ImageHeader, LogoProduct } from "../../assets";
import { WARNA_PUTIH } from "../../utils/constant";
import { Product } from "../../components";
import { Portfolio } from "../../components";

const Home = () => {
    return (
        <View style={styles.page}>
            <ImageBackground source={ImageHeader} style={styles.header}>
            </ImageBackground>
            
            <Product />
            <Portfolio />

        </View>
    )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    header: {
         width: windowWidth,
         height: windowHeight*0.41
    
    }
})
