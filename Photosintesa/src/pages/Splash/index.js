import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ImageBackground , Image } from 'react-native'
import { SplashBackground , Logo } from '../../assets'

const Splash = ({ navigation }) => {
    
    useEffect(() => {
        setTimeout( () => {
           navigation.replace('Welcome');
        }, 3000);
    });

    return (
    <ImageBackground source={SplashBackground} style={styles.background}> 
       <Image source={Logo} style={styles.Logo} /> 
    </ImageBackground>
    )
}

export default Splash

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    Logo: {
        width: 236,
        height: 233
    }
})
