import Home from './Home';
import Booking from './Booking';
import Profile from './Profile';
import Splash from './Splash';
import SignUp from './SignUp';
import Login from './Login';
import Welcome from './Welcome';

export { Splash, Profile, Booking, Home, SignUp, Login, Welcome }