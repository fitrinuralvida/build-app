import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Logo } from '../../assets'
import {WARNA_BIRU} from "../../utils/constant";
import ActionButton from './ActionButton';

const Welcome = ({navigation}) => {
    const handleGoto = (screen) => {
        navigation.navigate(screen);
    };
           return (
    <View style={styles.page}>
    
        <Image source={Logo} style={styles.Logo} /> 
    
    <ActionButton
    title="Get Started"
    onPress={() => handleGoto('SignUp')}
    />
    </View>
    );
};

export default Welcome

const styles = StyleSheet.create({
    Logo: {
        width: 236,
        height: 233,
        marginTop: 200
    },
    page: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    }
})
