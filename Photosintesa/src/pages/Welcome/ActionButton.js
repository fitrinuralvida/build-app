import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Logo } from '../../assets'
import {WARNA_BIRU} from "../../utils/constant";
import { Button } from "../../components";


const ActionButton = ({title, onPress}) => {
    return (
<View style={styles.page}>
    
    <Button title={title} onPress={onPress}/>
       
 </View>

);

};

const styles = {
    page: {
        marginTop: 200
    },
    button: {
        backgroundColor: WARNA_BIRU,
        borderRadius: 15,
        paddingVertical: 13,
        width: 241
    },
    getstarted: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        color: '#FFFFFF',
        textAlign: 'center'
    }
}

export default ActionButton;