import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Input } from "../../components/Atoms";
import { Button } from "../../components/Atoms";
import React, { useState } from 'react';
import { WARNA_BIRU } from "../../utils/constant";



const Login = (props) => {
    
    const [form, setForm] = useState({
        email: '',
        password: ''
    });
  
    const sendData = () => {
        console.log('data yang dikirim: ', form);
    };

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        });
    };
    return (
        
        <View style={styles.wrapper}>
        
        <Text style={styles.welcomeback}>Welcome Back</Text>
        <Text style={styles.loginto}>Login to Continue</Text>    

        <View style={styles.space(50)}  />  
        <Input placeholder="Email" value={form.email} onChangeText={(value) => onInputChange(value, 'email')}/> 
        <View style={styles.space(15)}  />
        <Input placeholder="Password" value={form.password} onChangeText={(value) => onInputChange(value, 'password')}
        secureTextEntry={true}
        /> 
        <View style={styles.space(40)}  />
        <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate('MainApp')}>
         <Text style={styles.getstarted} >
             Login
         </Text> 
     </TouchableOpacity> 
        
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    welcomeback: {
        color: '#FFFFFF',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 25,
        marginTop: 170,
        textAlign: 'center'
    },
    loginto: {
        color: '#FFFFFF',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 14,
        marginTop: 7,
        textAlign: 'center'
    },
    wrapper: {
        padding: 20,
        flex: 1
    },
    button: {
        backgroundColor: WARNA_BIRU,
        borderRadius: 15,
        paddingVertical: 13,
        width: 350,
        alignSelf: 'center'
    },
    getstarted: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        color: '#FFFFFF',
        textAlign: 'center'
    },
    space: value => {
        return {
            height: value,
        };
    },
    
});
