import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { DarkTheme } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { store } from './redux';


const App = () => {
    return (
      <Provider store={store}>
        <NavigationContainer theme={DarkTheme}>
          <Router />
        </NavigationContainer>
      </Provider>
        
    );
};

export default App

const styles = StyleSheet.create({})
