import Logo from './Logo.png';
import SplashBackground from './SplashBackground.png';
import ImageHeader from './Header.png';
import LogoProduct from './LogoProduct.png';
import ButtonPhotography from './ButtonPhotography.png';
import ButtonVideography from './ButtonVideography.png';
import ButtonDesign from './ButtonDesign.png';
import LogoPortfolio from './LogoPortfolio.png';
import Photo1 from "./Photo1.png";
import Photo2 from "./Photo2.png";
import Photo3 from "./Photo3.png";
import ProfileSignUp from "./ProfileSignUp.png";

export { Logo, SplashBackground, ImageHeader, LogoProduct, ButtonPhotography , ButtonVideography , ButtonDesign , LogoPortfolio , Photo1 , Photo2, Photo3, ProfileSignUp }