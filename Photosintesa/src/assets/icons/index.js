import IconBooking from './Booking.svg';
import IconBookingActive from './BookingActive.svg';
import IconHome from './Home.svg';
import IconHomeActive from './HomeActive.svg';
import IconProfile from './Profile.svg';
import IconProfileActive from './ProfileActive.svg';
import IconBack from './IconBack.svg';

export { IconBooking, IconBookingActive, IconHome, IconHomeActive, IconProfile, IconProfileActive, IconBack}