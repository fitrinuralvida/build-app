import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { IconBooking, IconBookingActive, IconHome, IconHomeActive, IconProfile, IconProfileActive } from "../../assets";
import { WARNA_UTAMA, WARNA_DISABLE } from "../../utils/constant";

const TabItem = ({ isFocused, onPress, onLongPress, label }) => {
    const Icon = () => {
        if(label === "Home") return isFocused ? <IconHomeActive/> : <IconHome />
    
        if(label === "Booking") return isFocused ? <IconBookingActive/> : <IconBooking />

        if(label === "Profile") return isFocused ? <IconProfileActive/> : <IconProfile />

        return <IconHome />
    }
    return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}>
            <Icon />    
            <Text style={styles.Text(isFocused)}>{label}</Text>
        </TouchableOpacity>
    );
};

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    Text: (isFocused) => ({
        fontSize: 10,
        color: isFocused ? WARNA_UTAMA : WARNA_DISABLE,
        marginTop: 5
    })
})
