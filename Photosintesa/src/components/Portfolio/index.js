import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import {WARNA_PUTIH} from "../../utils/constant";
import { LogoPortfolio , Photo1 , Photo2 , Photo3 } from "../../assets/images";

const Portfolio = () => {
    return (
        <View style={styles.container}>
            <Image source={LogoPortfolio} style={styles.IconPortfolio}>
            </Image>
            <Text style={styles.Portfolio}>Our Portfolio</Text>
            <Image source={Photo1} style={styles.Photo1}>
            </Image>
            <Image source={Photo2} style={styles.Photo2}>
            </Image>
            <Image source={Photo3} style={styles.Photo3}>
            </Image>
        </View>
    )
}

export default Portfolio

const styles = StyleSheet.create({
    Portfolio: {
        fontSize: 18,
        fontFamily: 'Montserrat-SemiBold',
        color: WARNA_PUTIH,
        marginLeft: 50,
        marginTop: -30
    },
    IconPortfolio: {
        marginTop: 8,
        marginLeft: 10
    },
    Photo1: {
        marginTop: 15
    },
    Photo2: {
        marginLeft: 150,
        marginTop: -208
    },
    Photo3: {
        marginLeft: 300,
        marginTop: -208
    }
})
