import React from 'react'
import { StyleSheet, Text, View , TextInput} from 'react-native'

const Input = ({placeholder, ...rest}) => {
    return (
        
            <TextInput style={styles.input} placeholder={placeholder} placeholderTextColor="white"
        {...rest} />
    )
}

export default Input

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 20,
        paddingVertical: 12,
        paddingHorizontal: 25,
        fontSize: 14,
        color: 'white',
        width: 350,
        alignSelf: 'center'
        

    }
})
