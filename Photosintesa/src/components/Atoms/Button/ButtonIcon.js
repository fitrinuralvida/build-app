import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconBack } from '../../../assets';

const ButtonIcon = ({...rest}) => {
    return (
        <TouchableOpacity {...rest}>
            {rest.name === 'back' && <IconBack width={49} height={19}/> }
        </TouchableOpacity>
        
    );
};

export default ButtonIcon;
