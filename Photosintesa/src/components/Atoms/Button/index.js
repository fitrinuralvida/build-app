import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text, StyleSheet } from 'react-native'
import { WARNA_BIRU } from "../../../utils/constant";
import { ButtonIcon } from './ButtonIcon'

const Button = ({title, onPress, type, name}) => {
    if(type === 'IconBack') {
       return <ButtonIcon name={name} onPress={onPress}/> ;
    }
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
         <Text style={styles.getstarted}>
            {title} 
         </Text>
     </TouchableOpacity> 
);

};

const styles = StyleSheet.create({
    button: {
        backgroundColor: WARNA_BIRU,
        borderRadius: 15,
        paddingVertical: 13,
        width: 350,
        alignSelf: 'center'
        
    },
    getstarted: {
        fontSize: 16,
        fontFamily: 'Montserrat-SemiBold',
        color: '#FFFFFF',
        textAlign: 'center'
    }
});
export default Button;
