import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import {WARNA_PUTIH} from "../../utils/constant";
import { LogoProduct , ButtonPhotography , ButtonVideography , ButtonDesign}  from "../../assets/images";

const Product = () => {
    return (
        <View style={styles.container}>
            <Image source={LogoProduct} style={styles.IconProduct}>
            </Image>
                <View style={styles.Text}>
        <Text style={styles.Product}>Our Product</Text>
            <Image source={ButtonPhotography} style={styles.IconPhotography}>
            </Image>  
            <Image source={ButtonVideography} style={styles.IconVideography}>
            </Image>  
            <Image source={ButtonDesign} style={styles.IconDesign}>
            </Image>            
        </View>
        </View>
    )
}


export default Product

const styles = StyleSheet.create({
    Product: {
        fontSize: 18,
        fontFamily: 'Montserrat-SemiBold',
        color: WARNA_PUTIH,
        marginTop: -30,
        marginLeft: 50
   
    },
    IconProduct: {
        marginTop: 8,
        marginLeft: 10
    },
    IconPhotography: {
        height: 42,
        width: 133.41,
        marginLeft: 12,
        marginTop: 13
    },
    IconVideography: {
        height: 42,
        width: 133.41,
        marginLeft: 153,
        marginTop: -42.5
    },
    IconDesign: {
        height: 42,
        width: 133.41,
        marginLeft: 293,
        marginTop: -42
    }
})
